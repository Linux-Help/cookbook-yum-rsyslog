default['yum']['rsyslog-devel']['repositoryid'] = 'rsyslog-devel'
default['yum']['rsyslog-devel']['enabled'] = false
default['yum']['rsyslog-devel']['managed'] = false
default['yum']['rsyslog-devel']['gpgkey'] = 'http://rpms.adiscon.com/RPM-GPG-KEY-Adiscon'
default['yum']['rsyslog-devel']['gpgcheck'] = true
default['yum']['rsyslog-devel']['release_repo'] = '8'

default['yum']['rsyslog-devel']['description'] = "Adiscon Rsyslog v#{node['yum']['rsyslog-devel']['release_repo']}-devel for CentOS-$releasever-$basearch"
default['yum']['rsyslog-devel']['baseurl'] = "http://rpms.adiscon.com/v#{node['yum']['rsyslog-devel']['release_repo']}-devel/epel-$releasever/$basearch"

