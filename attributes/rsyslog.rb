default['yum']['rsyslog']['repositoryid'] = 'rsyslog'
default['yum']['rsyslog']['enabled'] = true
default['yum']['rsyslog']['managed'] = true
default['yum']['rsyslog']['gpgkey'] = 'http://rpms.adiscon.com/RPM-GPG-KEY-Adiscon'
default['yum']['rsyslog']['gpgcheck'] = true
default['yum']['rsyslog']['release_repo'] = '8'

default['yum']['rsyslog']['description'] = "Adiscon Rsyslog v#{node['yum']['rsyslog']['release_repo']}-stable for CentOS-$releasever-$basearch"
default['yum']['rsyslog']['baseurl'] = "http://rpms.adiscon.com/v#{node['yum']['rsyslog']['release_repo']}-stable/epel-$releasever/$basearch"

